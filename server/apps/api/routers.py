from rest_framework import routers

from apps.test.viewsets import TestViewSet
from apps.users.viewsets import  UsersViewSet

router = routers.DefaultRouter()
router.register('test', TestViewSet, base_name='test')
router.register('users', UsersViewSet, base_name='users')
