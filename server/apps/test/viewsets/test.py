from rest_framework.viewsets import ModelViewSet
from rest_framework.viewsets import GenericViewSet
from rest_framework.viewsets import ReadOnlyModelViewSet

from apps.test.models import Test
from apps.test.serializers import TestSerializer
from rest_framework import generics, mixins, views

#class TestViewSet(ModelViewSet):
class TestViewSet(
                mixins.CreateModelMixin,
                mixins.ListModelMixin,
                mixins.RetrieveModelMixin,
                GenericViewSet):
    serializer_class = TestSerializer
    queryset = Test.objects.all()
